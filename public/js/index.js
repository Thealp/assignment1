//declaring variables
const balanceEl = document.getElementById("balance");
const getLoanEl = document.getElementById("getLoan");
const outstandingLoanEl = document.getElementById("outstandingLoanBox");
const payEl = document.getElementById("pay");
const bankButtonEl = document.getElementById("bankButton");
const workButtonEl = document.getElementById("workButton");
const repayLoanEl = document.getElementById("repayLoanButton");
const laptopsEl = document.getElementById("laptops");
const featuresEl = document.getElementById("featureBox");
const laptopNameEl = document.getElementById("laptopName");
const descriptionEl = document.getElementById("description");
const priceEl = document.getElementById("price");
const imageEl = document.getElementById("image");
const buyButtonEl = document.getElementById("buyButton");

let loanAmount = 0;
let balance = 100;
let payed = null;
let loan = false;
let outstandingLoan = null;
let pay = 0;

let laptops = [];

//Function for "get loan"-button
getLoanEl.addEventListener("click", function(){
    const amount = prompt("Enter amount");

    //If user cancel the prompt box, nothing should be returned
    if(amount === null){
        return;
    }

    //giving alert if input is twice the amount of the balance or is blank
    if(parseInt(amount) > (balance*2) || amount === ""){
        alert("cannot give you a loan");
    }
    //returning alert if user already has a loan
    else if(loan===true){
        alert("You can not get a new loan before you repay your last loan");
    }
    else
    {
        //adding input-amount to the balance variable
        balance += parseInt(amount);
        balanceEl.innerHTML = balance +" Kr";
        loan = true;
        loanAmount = amount;
        outstandingLoan = amount;

        // calling functions for repay loan and outstanding loan everytime a new loan is made
        repayLoanBtn();
        showOutstandingLoan();
    }
});

//function for "work"-button.
workButtonEl.addEventListener("click", function(){
    pay += 100;
    payEl.innerHTML = pay + " Kr";
});

//function for "bank"-button
bankButtonEl.addEventListener("click", function(){

    //10% og pay goes to loan. The rest goes to bank-balance.
    if(loan===true){
        let deducted = (pay / 100) * 10;
        outstandingLoan -= deducted;
        showOutstandingLoan();

        let toBank = pay - deducted;
        balance += toBank;
        balanceEl.innerHTML = balance + " Kr";

        pay = 0;
        payEl.innerHTML = pay + " Kr"; 
    }

    //the whole amount of pay goes to bank-balance.
    else
    {
        balance += pay;
        balanceEl.innerHTML = balance + " Kr";
        pay = 0;
        payEl.innerHTML = pay + " Kr"; 
    } 
});

//function for making the "repay loan"-button
function repayLoanBtn(){
    repayLoanEl.innerHTML= "";
    const html = `
        <button class="repayButton" id="repayButton">Repay Loan</button>
    `;
    repayLoanEl.insertAdjacentHTML("beforeend", html);
}

//function for making the "oustanding loan".section
function showOutstandingLoan(){
    outstandingLoanEl.innerHTML="";
    const html = `
        <p style="margin-right:25px;">Oustanding loan</p>
        <p id="oustandingLoan">${outstandingLoan} Kr</p>
     `;
     outstandingLoanEl.insertAdjacentHTML("beforeend", html);
}

//function for "repay loan"-button
repayLoanEl.addEventListener("click", function(){
    let remainingFund = pay - outstandingLoan;

    outstandingLoan -= pay;
    outstandingLoanEl.innerHTML="";
    showOutstandingLoan();

    payed += pay;
    pay = 0;
    payEl.innerHTML = pay + " Kr";

    //checking if loan is fully payed. The leftover pay will be transferred to bank
    if(parseInt(payed) >= loanAmount){
        loan=false;
        repayLoanEl.innerHTML= "";
        outstandingLoanEl.innerHTML="";
        payed = null;

        balance += remainingFund;
        balanceEl.innerHTML = balance + " Kr";
    }
});

//Retrieving data from RESTful API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToList(laptops));

//Adding laptops to list
const addLaptopsToList = (laptops) =>{
    laptops.forEach(x => addLaptopToList(x));
    featuresEl.innerHTML = laptops[0].specs;
    laptopNameEl.innerHTML = laptops[0].title;
    descriptionEl.innerHTML = laptops[0].description;
    priceEl.innerHTML = laptops[0].price + " NOK";
    imageEl.src = "https://noroff-komputer-store-api.herokuapp.com/"+laptops[0].image;
};

const addLaptopToList = (laptop) => {
    const laptopEl = document.createElement("option");
    laptopEl.value = laptop.id;
    laptopEl.appendChild(document.createTextNode(laptop.title));
    laptopsEl.appendChild(laptopEl); 
};

//function for displaying the data in dropdown-list
const handleLaptopListChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    featuresEl.innerHTML = selectedLaptop.specs;
    laptopNameEl.innerHTML = selectedLaptop.title;
    descriptionEl.innerHTML = selectedLaptop.description;
    priceEl.innerHTML = selectedLaptop.price + " NOK";
    imageEl.src = "https://noroff-komputer-store-api.herokuapp.com/"+selectedLaptop.image;
}

laptopsEl.addEventListener("change", handleLaptopListChange);

//function for "buy"-button.
const handleBuyButton = () => {
    const selectedLaptop = laptops[laptopsEl.selectedIndex];
    const laptopPrice = selectedLaptop.price;

    //checking if there is enough monkey in the bank
    if(balance < laptopPrice){
        alert("You cannot afford this laptop");
    }
    else{
        balance = balance - laptopPrice;
        balanceEl.innerHTML = balance + " Kr";
        alert("You are now the owner of the "+ selectedLaptop.title);
    }
}

buyButtonEl.addEventListener("click", handleBuyButton);

