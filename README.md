# Assignment1

# Table of contents
- Background
- Usage
- Author
- License

# Background
The Komputer Store project is part of the assignments assigned by Noroff. The task said to build a dynamic webpage using Javascript, HTML and CSS. The webpage had to contain a bank-section, a work-section and a laptop-section. The bank gives you the opportunity to get a loan, not more than twice your bank-balance. In the work-section you can earn money by pressing the work-button, and transfer these money to the bank by pressing the bank-button. If you have a loan you will have to repay this by pressing the repay loan-button. You can not get a new loan before this is done. The outstanding loan can be seen in the bank-section. 
The laptop-section contains a list of laptops retrieved from a RESTful API, information about the laptops, and a buy-button that can be used to buy a laptop. 

# Usage
To run the project you need to install Live Server in Visual Studio Code. Go to the html page, right click and click "Open with Live Server". The project should now run in your browser. 

# Author
Gitlab username: Thealp

Link: https://gitlab.com/Thealp 

# License
MIT © Thea Lunder Pettersen



